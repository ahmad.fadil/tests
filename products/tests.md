## 1. PRODUCTS APP → EDITOR: If sale price is equal to normal price, sale price field should be invalidated



## Result

Passed

## Used Data

user: ahmad.fadil@payever.org / password: Payever2023#

## Environment

Staging

- [payever] (commerceos.staging.devpayever.com)

Test cases

```
- Description

If sale price is equal to normal price, sale price field should be invalidated

**Steps**

1. Open products app
2. Click on add product
3. Fill all required by default fields
4. Enter sale price the same as normal price
5. Click Done


Status

**Passed**

Evidence

https://www.loom.com/share/2a58d7c8c60041a0bb0a30fa1430039b

```

---------------------

## 2. PRODUCTS APP → EDITOR: If sale price is more than normal price, sale price field should be invalidated



## Result

Passed

## Used Data

user: ahmad.fadil@payever.org / password: Payever2023#

## Environment

Staging

- [payever] (commerceos.staging.devpayever.com)

Test cases

```
- Description

If sale price is more than normal price, sale price field should be invalidated

**Steps**

1. Open products app
2. Click on add product
3. Fill all required by default fields
4. Enter sale price more than normal price
5. Click Done"


Status

**Passed**

Evidence

https://www.loom.com/share/2a58d7c8c60041a0bb0a30fa1430039b

```

---------------------



## 3. PRODUCTS APP → EDITOR: If sale price added, sale price date start and date end should be required


## Result

Passed

## Used Data

user: ahmad.fadil@payever.org / password: Payever2023#

## Environment

Staging

- [payever] (commerceos.staging.devpayever.com)

Test cases

```
- Description

If sale price added, sale price date start and date end should be required

**Steps**

1. Open products app
2. Click on add product
3. Fill all required by default fields
4. Fill sale price field
5. Click Done


Status

**Passed**

Evidence

https://www.loom.com/share/2a58d7c8c60041a0bb0a30fa1430039b

```

---------------------

## 4. PRODUCTS APP → EDITOR: Sale start date can not be later than sale end date



## Result

Passed

## Used Data

user: ahmad.fadil@payever.org / password: Payever2023#

## Environment

Staging

- [payever] (commerceos.staging.devpayever.com)

Test cases

```
- Description

Sale start date can not be later than sale end date

**Steps**

1. Open products app
2. Click on add product
3. Fill all required by default fields
4. Fill sale price field
5. Select sale end date
6. Select sale start date later than sale end date
7. Click Done


Status

**Passed**

Evidence

https://www.loom.com/share/2a58d7c8c60041a0bb0a30fa1430039b

```

---------------------

## 5. PRODUCTS APP → EDITOR: Sale information is saved



## Result

Passed

## Used Data

user: ahmad.fadil@payever.org / password: Payever2023#

## Environment

Staging

- [payever] (commerceos.staging.devpayever.com)

Test cases

```
- Description

Sale information is saved

**Steps**

1. Open products app
2. Click on add product
3. Fill all required by default fields
4. Fill sale price, sale start and end date
5. Click Done
6. After product saved click on product


Status

**Passed**

Evidence

https://www.loom.com/share/2a58d7c8c60041a0bb0a30fa1430039b

```

---------------------

## 7. PRODUCTS APP → EDITOR: Edit sale information




## Result

Passed

## Used Data

user: ahmad.fadil@payever.org / password: Payever2023#

## Environment

Staging

- [payever] (commerceos.staging.devpayever.com)

Test cases

```
- Description

Edit sale information

**Steps**

1. Open products app
2. Click on add product
3. Fill all required by default fields
4. Fill sale price, sale start and end date
5. Click Done
6. After product saved click on product
7. Edit sale price, sale start and end date
8. Click Done
9. After product saved click on product


Status

**Passed**

Evidence

https://www.loom.com/share/2a58d7c8c60041a0bb0a30fa1430039b


```

---------------------